#!/usr/bin/env nextflow 

/*
 * -------------------------------------------------
 * Main for GUIDANCE.v.2
 * -------------------------------------------------
 * Preprocessing managing and analysis initialization
 * 
 */

 
// DSL 2
nextflow.enable.dsl=2
version = '1.0'


include { PREPROCESSING } from './workflows/preprocessing/PREPROCESSING_DATA2.nf'
include { ANALYSIS }      from './workflows/analysis/GWAS_ANALYSIS.nf'


WorkflowMain.initialise(workflow, params, log)

workflow PREPROCESSING_DATA {

    PREPROCESSING()

}

workflow GWAS_ANALYSIS {

    ANALYSIS()

}

// workflow {

//     PREPROCESSING_DATA()
//     GWAS_ANALYSIS()
    
// }