#!/usr/bin/env bash

zcat !{ vcf } > !{filter_vcf}
zcat !{ vcf_index } > !{filter_vcf_index}