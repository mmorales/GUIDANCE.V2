#!/usr/bin/env bash
if !{chrom == 23}{

   echo "sbatch " !{all_analysis}
   echo "sbatch " !{female_analysis}
   echo "sbatch " !{male_analysis}


}else {

    echo "sbatch " !{slurm_script}
}