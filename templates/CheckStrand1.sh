#!/usr/bin/env bash

bash !{mainDir}/bin/QC_starnad/00_Format_QC.sh \
     !{studyPath}/!{sampleID} \
     !{preprocessingDir}/QC_forGuidance \
     !{preprocessingDir}/logs_qc_for_guidance \
     hg19
