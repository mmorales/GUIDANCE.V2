#!/usr/bin/env bash

module load bcftools
./!{plinkBin} --bfile !{sampleID}  --keep-allele-order --recode vcf bgz --out !{sampleID}
bcftools index !{sampleID}.vcf.gz -t
