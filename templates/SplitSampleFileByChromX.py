#!/usr/bin/env python

import os
import pandas as pd
 

class FamFromPlinkFile:

    def __init__(self, sampleID, fam_path):
        self.sampleID = sampleID
        self.fam_df = pd.read_csv(fam_path, sep = " ", usecols=[0,1], names=['ID_1', 'ID_2'] )


class SampleByChromX(FamFromPlinkFile):

    def __init__(self, sampleID, fam_df, studyID, sample_file):
        super().__init__(sampleID, fam_df)
        self.studyID = studyID
        self.sample_df = pd.read_csv(sample_file, sep = " ")


class NewSampleFile(SampleByChromX, FamFromPlinkFile):

    def __init__(self, studyID, fam_df, sampleID, sample_df, sex):
        super().__init__(studyID, fam_df, sampleID, sample_df)
        self.sex = sex

    def get_sample_header(self):
        self.first_line = self.sample_df.iloc[0:1]
     
    def get_common_patients(self):
        tmp_sample_df = self.sample_df[1:].reset_index(drop=True)
        self.filtered_df = tmp_sample_df.merge(self.fam_df, 
			                       how = 'inner', 
					       on = ['ID_1', 'ID_2'])
        
    def write_outfile(self):
        new_sample = self.first_line.append(self.filtered_df, ignore_index = True)
        new_sample.to_csv (f"{self.sex}_{self.sampleID}.sample", sep = " ", index = False, header=True)
	


def main(sample_file: str, plink_file_fam:str, sex:str) -> pd.DataFrame:
    """ Create a New Sample file for the individuald who belongs 
        to an specific sex depending on the X chromsomome 
    """
    cohort = sample_file.split('.')[-2]
    sample_tag = plink_file_fam.split('.')[-2]

    new_sample_file = NewSampleFile(cohort, plink_file_fam, sample_tag, sample_file, sex)
    
    new_sample_file.get_sample_header()
    
    new_sample_file.get_common_patients()
    
    new_sample_file.write_outfile()



if __name__== "__main__":
    # Files to filter
    sample_file = "!{sample}"
    fam_file = "!{fam}"
    sex = "!{sex}"

    #sex = 'females'
    #sample_file = "../../guidance.v.2/dbgap/phs000346.v2.p2/phs000346.sample"
    #fam_file = "../outputs/phs000346.v2.p2/preprocessing/logs_qc_for_guidance/work/ca/2859ffb3eb64bdb864ef457c1207b0/females_phs000346v2p2_QC_forGuidance_hg19_strplus.fam"
    
    main(sample_file, fam_file, sex)
