#!/usr/bin/env bash

./!{plinkBin} --vcf !{vcf} --frqx --keep-allele-order --out !{sampleID}
