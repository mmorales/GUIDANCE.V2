#!/usr/bin/env python

import os 
import sys
import pandas as pd


class Frecuencies():

    columns_head = ['CHR','SNP', 'A1', 'A2', 'C(HOM A1)', 'C(HET)',  
                    'C(HOM A2)', 'C(HAP A1)', 'C(HAP A2)', 'C(MISSING)']

    def __init__(self, chrom, frqx_path):
        self.chromsosome = chrom
        self.data = pd.read_csv(frqx_path, sep = "\t", header=[0])
        

    def __str__(self):
        return 'finding missings in chromosome: ' + self.chromsosome 


    @classmethod
    def columns(cls):
        return cls.columns_head


    def get_missing_percentage(self, colums):
        
        self.data['total'] = self.data[colums].sum(axis=1)
        self.data['missing_%'] = (self.data['C(MISSING)']/self.data['total']) * 100

        return self.data[['SNP', 'missing_%']]
        

    def write_outfile(self, missing_df):
        missing_df.to_csv (f"snps_to_filter_{self.chromsosome}.txt", sep = "\t", index = False, header=False)




def main(chrom, file_path):

    freq_df = Frecuencies(chrom,file_path)   

    colums_to_sum = freq_df.columns()[4:]

    missing_percentage = freq_df.get_missing_percentage(colums_to_sum)

    missingIDs = missing_percentage.loc[missing_percentage['missing_%'] > 5]['SNP']
    
    freq_df.write_outfile(missingIDs)



if __name__ == "__main__":
    #freq_path = sys.argv[1]
    chromosome = "!{chromosome}"
    freq_path = "!{frqx}"
    main(chromosome, freq_path)