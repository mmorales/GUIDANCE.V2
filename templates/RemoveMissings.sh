module load bcftools
bcftools view --exclude ID==@!{missingRsList} !{vcf} -Oz -o !{filter_vcf}
bcftools index !{filter_vcf} 