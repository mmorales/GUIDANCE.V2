#!/usr/bin/env bash

module load bcftools

./!{impute5Bin}  --h !{panel} --g !{phased} --m !{map} --r !{impute} --o imputed_chunks_chr!{impute}_!{chromtype}.vcf.gz --l imputed_chunks_chr!{impute}_!{chromtype}.log  --threads !{CPUS} --out-gp-field --buffer-region !{buffer}
bcftools index imputed_chunks_chr!{impute}_!{chromtype}.vcf.gz

