#!/usr/bin/env bash

log_dir="!{preprocessingDir}/logs_qc_for_guidance"
work_path="!{preprocessingDir}/QC_forGuidance"
#plink_strand="!{sampleID}_forGuidance_hg19_strplus"
hg="hg19"

grep TEMP Run-plink.sh > Run-plink_strand_corr.sh
sed -i 's|--bfile '!{sampleID}'|--bfile '${work_path}'/'!{sampleID}'|g' Run-plink_strand_corr.sh
sed -i 's|Exclude|'${work_path}'/Exclude|g' Run-plink_strand_corr.sh
sed -i 's|Chromosome|'${work_path}'/Chromosome|g' Run-plink_strand_corr.sh
sed -i 's|Position|'${work_path}'/Position|g' Run-plink_strand_corr.sh
sed -i 's|Strand|'${work_path}'/Strand|g' Run-plink_strand_corr.sh
sed -i 's|Force|'${work_path}'/Force|g' Run-plink_strand_corr.sh

#sed -i 's|--bfile TEMP|--bfile '${work_path}'/TEMP|g' Run-plink_strand_corr.sh

mv $log_dir/*-HRC.txt $work_path
if [ $hg != "hg19" ]; then
	sed -i 's/_hg19-updated/_hg19_strplus/' Run-plink_strand_corr.sh
else
	sed -i 's/-updated/_hg19_strplus/' Run-plink_strand_corr.sh
fi
sed -i 's/plink/\/home\/bsc64\/bsc64212\/apps\/plink1.9\/plink/'  Run-plink_strand_corr.sh
#sed -i 's/--bfile /--bfile"$work_

bash Run-plink_strand_corr.sh

echo "Strand correction done"
echo "Final output $work_path/!{plink_strand}"
