#!/usr/bin/env bash

module load bcftools

bcftools +fill-tags !{imputed_vcf} -Oz -o annotated_!{imputed_vcf}
bcftools view -i 'INFO/INFO>=0.7 && INFO/MAF>0.001' annotated_!{imputed_vcf} -Oz -o
bcftools index annotated_!{imputed_vcf}
