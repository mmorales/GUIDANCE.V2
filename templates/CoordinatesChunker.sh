#!/usr/bin/env bash

module load bcftools

set -e

{
    ./!{impute5Chuncker} --h !{panel_id}".vcf.gz" --g !{phased_id}".vcf.gz" --r !{chromosome} --o coordinates_chr!{chromosome}_!{chromtype}.txt
    #./!{impute5Chuncker} --h !{panel_vcf} --g !{phased_vcf} --r !{chromosome} --o coordinates_chr!{chromosome}_!{chromtype}.txt
    # awk -F'\t' '{print $3}' coordinates_chr!{chromosome}.txt > buffer_regions_chr!{chromosome}.txt
    # awk -F'\t' '{print $4}' coordinates_chr!{chromosome}.txt > imputed_regions_chr!{chromosome}.txt


    awk -F'\t' '{print $1"\t"$3"\t"$4}' coordinates_chr!{chromosome}_!{chromtype}.txt > regions_chr!{chromosome}_!{chromtype}.txt


} || {

    touch coordinates_chr!{chromosome}_!{chromtype}.txt
    touch regions_chr!{chromosome}_!{chromtype}.txt

}
