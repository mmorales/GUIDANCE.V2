#!/usr/bin/env bash

module load bcftools
bcftools view --exclude ID==@!{pair_list} !{vcf} -Oz -o  !{filter_vcf}
bcftools index !{filter_vcf} -t 