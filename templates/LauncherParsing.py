#!/usr/bin/env python

import os 
import sys
import json

class ParseCMD:

    script_template = '\n'.join([
        '#!/usr/bin/env bash',
        '#SBATCH --job-name=analysis_launcher_chr{chromosome}_{status}',
        '#SBATCH --workdir=.',
        '#SBATCH --output={root}/slurm_logs/%x_%j.out',
        '#SBATCH --error={root}/slurm_logs/%x_%j.err',
        '#SBATCH --ntasks=1',
        '#SBATCH --cpus-per-task={cpus}',
        '#SBATCH --qos=bsc_ls',
        '#SBATCH --time={time}',
        '',
        './nextflow run {root}/main.nf \\',
                 '-c {root}/profiles.config \\',
                 '-w {root}/work/chrom{chromosome}_{status} \\', 
                 '-profile local_binaries \\',
                 '-entry GWAS_ANALYSIS \\',
                 '-resume \\',
                 '--chromosome {chromosome} \\',
                 '--CPUS {cpus} \\',
                 '--TIME {time} \\',
                 '--chrom_type {status} \\',
                 '--pipeline GWAS_ANALYSIS'
    ])
    
    # head_template.format(chromosome = "!{chromsomeRange}",
    #         cpus=chrom_cpus,
    #         time=chrom_time
    # )


    def __init__(self, study, root, out, chrom):
        """ """

        self.study_id = study 
        self.rootdir = root
        self.outdir = out
        self.chromosome = chrom
     

    def __str__(self):
        """ """
        return f"preparing slurm commands for chromosome: {self.chromsosome}"


    # @classmethod
    # def header(cls):
    #     return cls.head_template


    def get_resources(self, resource_config):
        """ """

        with open(resource_config) as f:
            data = f.read()
        js = json.loads(data)

        chrom_cpus = js["chr" + self.chromosome]['cpus']
        chrom_time = js["chr" + self.chromosome]['time']

        return chrom_cpus, chrom_time


    def define_execution(self, chrom_cpus, chrom_time, status):
        """"""

        return ParseCMD.script_template \
                       .format(chromosome = self.chromosome, 
                               study = self.study_id,
                               root = self.rootdir,
                               cpus = chrom_cpus,
                               time = chrom_time,
                               status = status 
                        )

    def write_script(self, body_text, status):
        """"""

        launchers_dir = os.path.join(self.outdir, "launchers")
        os.makedirs(launchers_dir, exist_ok=True)

        launcher = f"analysis_chr{self.chromosome}_{status}.cmd"
        if not os.path.exists(launcher):
            with open(launcher, 'w') as f:
                f.write(body_text)


def main():
    """ """

    study        = "!{study}"
    analysis_dir = "!{analysisDir}"
    root_dir     = "!{rootPath}"
    json_config  = "!{json}"
    chrom        = "!{chromsomeRange}"

    # study        = "phs"
    # analysis_dir = "../"
    # root_dir     = "/gpfs/projects/bsc64/bsc64212/guidance.v2"
    # json_config  = "/gpfs/projects/bsc64/bsc64212/guidance_v2/config/chromCPUs.json"
    # chrom        = "23"


    cmd_script = ParseCMD(study, root_dir, analysis_dir, chrom)

    cpus, time = cmd_script.get_resources(json_config)


    cmd_content = cmd_script.define_execution(cpus, time, "all")
    cmd_script.write_script(cmd_content, "all")

    if chrom == "23" or chrom == "X":

        cmd_content_female = cmd_script.define_execution(cpus, time, "female")
        cmd_script.write_script(cmd_content_female, "female")

        cmd_content_male = cmd_script.define_execution(cpus, time, "male")
        cmd_script.write_script(cmd_content_male, "male")
        

if __name__== "__main__":
    main()
