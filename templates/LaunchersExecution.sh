#!/usr/bin/env bash

if [ -f "!{slurm_script}" ]; then
    echo "vcf exists. Launching analysis..."
    sbatch !{slurm_script}
fi


