#!/usr/bin/env bash

module load bcftools

if [ !{tooltype} == "binaries" ];
then

    ./!{shapeit4Bin} --input !{sampleid}.vcf.gz --map !{map} --region !{chromosome} --output phased_chr!{chromosome}_!{chromtype}.vcf.gz --log phased_chr!{chromosome}_!{chromtype}.log  --thread !{CPUS}
    bcftools index phased_chr!{chromosome}_!{chromtype}.vcf.gz

fi


if [ !{tooltype} == "slurm" ];
then 

    module load shapeit4

    shapeit4 --input !{sampleid}.vcf.gz --map !{map} --region !{chromosome} --output phased_chr!{chromosome}_!{chromtype}.vcf.gz --log phased_chr!{chromosome}_!{chromtype}.log  --thread !{CPUS}
    bcftools index phased_chr!{chromosome}_!{chromtype}.vcf.gz

fi