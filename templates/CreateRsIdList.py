#!/usr/bin/env python

import os
import pandas as pd

class BimFile:
    
    df_names = [
        "Chromosome code",
        "Variant identifier",
        "cM position",
        "Base-pair coordinate",
        "Allele 1",
        "Allele 2"
    ]

    def __init__(self, file:str):
        self.bim_data = file

    # Cargar archivo en el dataframe
    def df_data(self)-> None:
        try: 
            self.df = pd.read_csv(self.bim_data, header=None, sep='\t', names=BimFile.df_names)
        except ValueError:
            print("""Error, It was not possible to generate the .pairs file.
                     The format is not valid. 
                  """)

	    
class CreateRsIdsFile(BimFile):

    # SNPs of interest to locate in bim
    dict_alleles = {"A/T":["A","T"],
                    "T/A":["T","A"],
                    "G/C":["G","C"],
                    "C/G":["C","G"]
                }


    def __init__(self, bim_data:str):
        super().__init__(bim_data)
        self.outFile = "GCAT_IDs_list.pairs"


    # List of SNPs that match with the alleles in dict
    def check_ATCG_alleles(self) -> None:
        super().df_data()
	
        dict_rs = {}
        alleles = CreateRsIdsFile.dict_alleles
        for allele_pair in alleles.keys():
            allele_A = alleles[allele_pair][0]
            allele_B = alleles[allele_pair][1]
	
            df_filtered = self.df[(self.df["Allele 1"]==allele_A) &
			          (self.df["Allele 2"]==allele_B)]["Variant identifier"]
            dict_rs[allele_pair] = df_filtered.tolist()

        self.rs_id_list = [rs for k in dict_rs.keys() for rs in dict_rs[k]]


    # Write RsIdList to file
    def create_outfile(self) -> None:
        f = open(self.outFile,"w")
        for rs in self.rs_id_list:
                f.write(rs + "/n")
                #f.write("")
        f.close()



def main(bim_file:str)-> None:
    
    # Instance object CreateRsIdsFile
    bim = CreateRsIdsFile(bim_file)
    
    # Find A/T C/G alleles in bim file
    bim.check_ATCG_alleles()
   
    # write list with IDs of founded SNPs
    bim.create_outfile()



if __name__ == "__main__": 
    #bim_file ="/gpfs/scratch/bsc64/bsc64212/labtest/inputs/phs001315.v1.p1/phs001315_QC.bim"
    bim_file = "!{bim}"
    main(bim_file)

