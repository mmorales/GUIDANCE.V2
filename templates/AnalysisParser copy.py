#!/usr/bin/env python

import re
import os 
import sys
import json


def main():

    #preprocessing_dir = sys.argv[1]
    preprocessing_dir = "!{preprocessingDir}"
    chrom             = "chr"+"!{chromsomeRange}"


    resource_config = "/gpfs/projects/bsc64/bsc64212/guidance_v2/config/chromCPUs.json"
    with open(resource_config) as f:
        data = f.read()

    js = json.loads(data)

    #for chrom in js:
    
    chrom_cpus = js[chrom]['cpus']
    chrom_time = js[chrom]['time']

    head_template = """#!/usr/bin/env bash
#SBATCH --job-name=analysis_launcher_def{chromosome}
#SBATCH --workdir=/gpfs/projects/bsc64/bsc64212/guidance_v2
#SBATCH --output=outputs/phs000346.v2.p2/analysis/logs/%x_%j.out
#SBATCH --error=outputs/phs000346.v2.p2/analysis/logs/%x_%j.err
#SBATCH --ntasks=1
#SBATCH --cpus-per-task={cpus}
#SBATCH --qos=bsc_ls
#SBATCH --time={time}

module load nextflow/21.04.1
nextflow run /gpfs/projects/bsc64/bsc64212/guidance.v2/launch_analysis.nf \
                -c /gpfs/projects/bsc64/bsc64212/guidance.v2/config/base.config \
                -resume \
                --chromosome {chromosome} \
                --CPUS "{cpus}" \
                --TIME "{time}"
    """.format(chromosome = "!{chromsomeRange}", cpus=chrom_cpus,time=chrom_time)

    launchers_dir = os.path.join(preprocessing_dir, "launchers")
    if not os.path.exists(launchers_dir):
        os.makedirs(launchers_dir)


    #launcher = os.path.join(launchers_dir, "analysis_"+chrom+".cmd")
    launcher = "analysis_"+chrom+".cmd"
    with open(launcher, 'w') as f:
        f.write(head_template)
        f.close()
        

if __name__== "__main__":
    main()
