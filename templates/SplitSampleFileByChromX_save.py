import os
import pandas as pd

# class SampleByChromX:

#     def __init__(self, cohort, sample_file):
#         self.cohort = cohort
#         self.sample_df = pd.read_csv(sample_file, sep = " ", header = [0,1])
    

# class FamFromPlinkFile:

#     def __init__(self, sampleID, fam_path):
#         self.sampleID = sampleID
#         self.fam_df = pd.read_csv(fam_path, sep = "", usecols=[0,1], names=['ID_1', 'ID_2'] )


# class NewSampleFile(SampleByChromX, FamFromPlinkFile):

#     def __init__(self, ):
#         SampleByChromX.__init__(self, cohort)

#     def filter_common_patients(self):
#         print(self.fam_df)
#         print(self.sample_df)
#         print(self.cohort)
#         print(self.sampleID)




def main(sample_file: str, plink_file_fam:str, sex:str) -> pd.DataFrame:
    """ 
    Create a New Sample file for the individuald who belongs 
    to an specific sex depending on the X chromsomome 
    """

    cohort = sample_file.split('.')[0]

    sample_df = pd.read_csv(sample_file, sep = " ")
    fam_df = pd.read_csv(plink_file_fam, sep = " ", usecols=[0,1], names=['ID_1', 'ID_2'] )
    
    first_line = sample_df.iloc[0:1] 
    tmp_sample_df = sample_df[1:].reset_index(drop=True)

    filtered_df = tmp_sample_df.merge(fam_df, how = 'inner', on = ['ID_1', 'ID_2'])

    new_sample = first_line.append(filtered_df, ignore_index = True)
    new_sample.to_csv (f"{sex}.sample", sep = " ", index = False, header=True)


if __name__== "__main__":
    # Files to filter
    sample_file = "!{sample}"
    fam_file = "!{fam}"
    sex = "!{sex}"

    # sex = 'females'
    # sample_file = "../../guidance.v.2/dbgap/phs000346.v2.p2/phs000346.sample"
    # fam_file = "../outputs/phs000346.v2.p2/preprocessing/logs_qc_for_guidance/work/ca/2859ffb3eb64bdb864ef457c1207b0/females_phs000346v2p2_QC_forGuidance_hg19_strplus.fam"
    
    main(sample_file, fam_file, sex)