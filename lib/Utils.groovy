

class Utils {


    //public static int[] stepsValidation(params, log) {
    public static int stepsValidation(params, log) {

        def step = params.wfDeep

        // def init_step = params.from_step
        // def end_step = params.until_step
        
        def allSteps = ["until_Check_Strand", 
                        "until_Split_Chromosomes", 
                        "until_Check_Chrom_X", 
                        "until_Recode_vcf", 
                        "until_Filter_ATCG_and_Missings",
                        "until_parse_launcher_analysis",
                        "until_launch_analysis",
                        "until_phasing",
                        "until_imputation",
                        "until_filter_MAF_and_INFO",
                        "until_merging"
                    ]

        def index = allSteps.findIndexOf { it == step }

        // def init_index = allSteps.findIndexOf { it == init_step }
        // def end_index = allSteps.findIndexOf { it == end_step }

        
        return index
        //return [init_index, end_index]

    }

    public static void checkAndMakeDirs(paths, log){
        
        for (String path in paths){
           
            try { 

                def dir = new File(path)

                if (! dir.exists()){
                    log.info"Creating directory: ${dir.name}"
                    dir.mkdirs()
                    
                }
            
            } catch(Exception e) {

                log.warn"It wasn't able to create the folder"
            }
        }
        log.info"All folders were created"
    }

    public static pathsTree(workStep, mainDirs, params, log) {

        def subfolders_steps = ["splited_chromosomes", "recodedVCFs"]

        def outDir = params.paths.outputPath
        def study  = params.cohort.study
        def init_chrom = params.init_chrom as Integer
        def end_chrom  = params.end_chrom as Integer
        def paths_list = []

        for (String step in mainDirs){

            def dir = new File("${outDir}/${study}/${workStep}/${step}")
            paths_list.add(dir)

            if (subfolders_steps.contains(step)){
            //if (step == "splited_chromosomes" | step == "recodedVCFs") {
                for (String chr in init_chrom..end_chrom){

                    //def subdir = new File("${outDir}/${study}/${workStep}/${step}/chrom${chr}")
                    def subdir = new File("${dir}/chrom${chr}")
                    paths_list.add(subdir)
                }
            }
        }
        return paths_list
    }


    public static void createDirectories(params, log){

        def preprocessing = [ "logs_qc_for_guidance",
                              "QC_forGuidance",
                              "splited_chromosomes",
                              "recodedVCFs",
                              "launchers"
                            ]

        def analysis = ["logs",
                        "phasing",
                        "imputation",
                        "filterInfoAndMaf",
                        "merging"
                        ]
        

        if (params.pipeline == "PREPROCESSING_DATA"){

            def dirsToCreatePrepDirs = pathsTree("preprocessing", preprocessing, params, log)
            checkAndMakeDirs(dirsToCreatePrepDirs, log)

        }
        
        if (params.pipeline == "GWAS_ANALYSIS"){

            def dirsToCreateAnaDirs = pathsTree("analysis", analysis, params, log)
            checkAndMakeDirs(dirsToCreateAnaDirs, log)

        }

    }

}
