
class WorkflowMain {


    static String helpMessage() { 

        """
            Usage:
            The typical command for running the pipeline is as follows:

            nextflow run         rootpath/main.nf \\
                    -c           rootpath/profiles.config \\
                    -w           rootpath/work/chrom
                    -profile     local_binaries \\
                    -entry       GWAS_ANALYSIS \\
                    --pipeline   GWAS_ANALYSIS

            (Only in GWAS_ANALYSIS)

                    --chromosome 22 \\
                    --CPUS       48 \\
                    --TIME       02:00:00 \\
                    --chrom_type all \\
                    

            Mandatory arguments in PREPROCESSING_DATA AND GWAS_ANALYSIS:

                -c,C [file]           Configuration file of the process
                -w   [directory]      Place where are goint to be stored all tmp files 
                -profile [str]        Profile to execute and launch specific configuration
                --pipeline [str]      Inicates the part of the analysis we are executing


            Specific arguments for GWAS_ANALYSIS:

                --chromosome [str]    Analysis executed for the specified chromosome        
                --CPUS [str]          Number of cores used to the process
                --TIME [str]          Timelimit to execute GWAS_ANALYSIS
                --chrom_type [str]    Parameter used to distinguish between partial and 
                                      complete chromosome 23
                
        """.stripIndent()

    }

    static String citation(workflow) { 
       "If you use ${workflow.manifest.name} for your analysis please cite:\n\n" +
       "* The Original Guidance Proyect\n" +
       "  https://doi.org/10.1038/s41467-021-21952-4\n\n" +
        "* The current helpMessageGuidance version\n" +
        "  https://git/\n\n" 
       
    }

    public static String initMessage(params) {
        """
        G U I D A N C E - V . 2 . 1
        ===================================
        STUDY     : ${params.cohort.study}
        PANELS    : ${params.refpanel_names}
        TEST_TYPE : ${params.cohort.test_types}
        PIPELINE  : ${params.pipeline}
        wfDeep    : ${params.wfDeep}
        """.stripIndent()

    }

    public static void initialise(workflow, params, log) {

        // Print help to screen if required
        if (params.init_message) {
            log.info initMessage(params)
            //System.exit(0)
        }

        if (params.citation) {
            log.info citation(workflow)
            //System.exit(0)
        }

        if (params.helpmessage){
            log.info helpMessage()
           exit 0
        }

        Utils.createDirectories(params, log)

  
        // Validate workflow parameters via the JSON schema
        // if (params.validate_params) {
            
        //     //ValidateParameters.stepsValidation(workflow, params, log)
        // //     NfcoreSchema.validateParameters(workflow, params, log)
        // }


    }
}

