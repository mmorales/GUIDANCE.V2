
public class InfoMessages {

    def initMessage () {
       log.info"""
     G U I D A N C E - V . 2 . 1
    ===================================
    STUDY     : ${params.cohort.study}
    PANELS    : ${params.refpanel_names}
    TEST_TYPE : ${params.test_types}
    wfDeep    : ${params.wfDeep}
    wfDeep2   : ${params.wfDeep2}
    """.stripIndent()
    }

    public static String initMessage2() {
        init_message = """
        G U I D A N C E - V . 2 . 1
        ===================================
        STUDY     : ${params.cohort.study}
        PANELS    : ${params.refpanel_names}
        TEST_TYPE : ${params.test_types}
        wfDeep    : ${params.wfDeep}
        wfDeep2   : ${params.wfDeep2}
        """.stripIndent()

        return init_message
    }

    public static String helpMessage() {
         def help_string = "prueba"
         return help_string
    }
    
    def manifest() {
        log.info"""
    """.stripIndent()
    }

    def citation() {
        log.info"""
    """.stripIndent()
    }

    def warnings() {
        log.info"""
    """.stripIndent()
    }

}