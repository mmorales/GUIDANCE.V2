//import GuidanceUtilities.createDir

class WorkflowMain2 {

    static String citation(workflow) { 
       "If you use ${workflow.manifest.name} for your analysis please cite:\n\n" +
       "* The Original Guidance Proyect\n" +
       "  https://doi.org/10.1038/s41467-021-21952-4\n\n" +
        "* The current Guidance version\n" +
        "  https://git/\n\n" 
       
       }

    public static String initMessage(params) {
        """
        G U I D A N C E - V . 2 . 1
        ===================================
        STUDY     : ${params.cohort.study}
        PANELS    : ${params.refpanel_names}
        TEST_TYPE : ${params.cohort.test_types}
        wfDeep    : ${params.wfDeep}
        """.stripIndent()
    }

    public static void initialise(workflow, params, log) {

        // Print help to screen if required
        if (params.init_message) {
            log.info initMessage(params)
            System.exit(0)
        }

       if (params.citation) {
            log.info citation(workflow)
            System.exit(0)
        }

        // Validate workflow parameters via the JSON schema
        // if (params.validate_params) {
        //     NfcoreSchema.validateParameters(workflow, params, log)
        // }

    }


}

