
process CoordinatesChunker{

    cpus "${CPUS}"

    publishDir "${imputeDir}/chrom${chromosome}/coordinates", 
    mode: 'copy',
    overwrite: true 

    input:
        path(impute5Chuncker)
        val(chromtype)
        val(tooltype)
        val(imputeDir)
        tuple val(chromosome), val(panel_id), path(panel_vcf), path(panel_csi)
        tuple val(chromosome), val(phased_id), path(phased_vcf), path(phased_csi)
        val(CPUS)

    output:
        path "coordinates_chr${chromosome}_${chromtype}.txt"
        path "regions_chr${chromosome}_${chromtype}.txt", emit: regions 

    shell:
        template "CoordinatesChunker.sh"

}

process Imputation{

    cpus "${CPUS}"

    publishDir "${imputeDir}/chrom${chromosome}", 
    mode: 'copy',
    overwrite: true 
    maxForks 1

    input:
        path(impute5Bin)
        val(chromtype)
        val(tooltype)
        val(imputeDir)
        val(CPUS)
        tuple val(chromosome), val(panel_id), path(panel), path(panel_csi), path(map), val(phasing_id), path(phased), path(phased_csi), val(chunkID), val(buffer), val(impute)

    output:
        tuple val("${chromosome}"), val(imputed_id), path(imputed_vcf), path(imputed_csi), emit: imputed_vcf
        tuple val("targ"), path("*targ_only_variants.gz") 
        path(imputed_log)
        

    shell:
        imputed_id = "imputed_chunks_chr${impute}_${chromtype}"
        imputed_vcf = "${imputed_id}.vcf.gz"
        imputed_csi = "${imputed_id}.vcf.gz.csi"
        imputed_log = "${imputed_id}.log"
        //not_imputed_vcf = "imputed_chunks_chr${impute}_${chromtype}.targ_only_variants.gz"
        
        template "Imputation.sh"

}
