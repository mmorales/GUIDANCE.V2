#!/usr/bin/env nextflow 

process filter_maf_and_info {

    cpus "${CPUS}"

    publishDir "${analysisDir}/filterInfoAndMaf", 
    mode: 'copy',
    overwrite: true 

    input:
        val chromtype
        val tooltype
        val analysisDir
        tuple val(chromosome), path(imputed_vcf), path(imputed_csi)
        val CPUS

    output:
         tuple val(chromosome), path(filtered_vcf), path(filtered_csi)

    shell:
        filtered_vcf = "annotated_${imputed_vcf}"
        filtered_csi = "annotated_${imputed_vcf}.csi"

        template 'Filter_maf_and_info.sh'

}