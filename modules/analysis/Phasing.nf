#!/usr/bin/env nextflow 

process Phasing {

    cpus "${CPUS}"

    publishDir "${analysisDir}/phasing", 
    mode: 'copy',
    overwrite: true 

    input:
        path shapeit4Bin
        val chromtype
        val tooltype
        val analysisDir
        tuple val(chromosome), val(sampleid), path(vcf), path(csi)
        tuple val(chromosome), path(map)
        val CPUS

    output:
        tuple val(chromosome), val(new_sampleid), path(phased_vcf), path(phased_csi), emit: phased_file
        path(phased_log)

    shell:

        new_sampleid = "phased_chr${chromosome}_${chromtype}"
        phased_vcf = new_sampleid + ".vcf.gz"
        phased_csi = new_sampleid + ".vcf.gz.csi"
        phased_log = new_sampleid + ".log"
 
        template 'Phasing.sh'




}