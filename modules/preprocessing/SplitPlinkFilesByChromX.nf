process SplitPlinkFilesByChromX {

    publishDir "${preprocessingDir}/chromInputs", 
        mode: 'copy',
        overwrite: true 

    input:
    val sex
    path plinkBin
    val preprocessingDir
    tuple val(chromosome), val(sampleID), path(bim), path(bed), path(fam)

    when:
    chromosome == '23'

    output:
    tuple val(chromosome), val("${sex_sample}"), path("${sex_sample}.bim"), path("${sex_sample}.bed"), path("${sex_sample}.fam")

    shell: 
    sex_sample = "${sex}_${sampleID}"
    template 'SplitPlinkFilesByChromX.sh'

}

