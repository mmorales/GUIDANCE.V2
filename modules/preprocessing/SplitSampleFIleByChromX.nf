process SplitSampleFileByChromX {

    publishDir "${preprocessingDir}/chromInputs", 
        mode: 'copy',
        overwrite: true 

    input:
    val sex
    path sample
    val preprocessingDir
    tuple val(chromosome), val(sampleID), path(bim), path(bed), path(fam)

    when:
    chromosome == '23'

    output:
    tuple val("23"), path("${sex}_${sample}")

    shell:
    template 'SplitSampleFileByChromX.py'

}