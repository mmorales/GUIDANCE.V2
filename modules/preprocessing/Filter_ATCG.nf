process CreateRsIdList {

    publishDir "${preprocessingDir}/recodedVCFs", 
        mode: 'copy',
        overwrite: true

    input:
        val preprocessingDir
        tuple val(sampleID), path(bim), path(bed), path(fam) 

    output:
        path 'GCAT_IDs_list.pairs'

    // when:
    // exclude_cgat_snps == true

    shell:
        template "CreateRsIdList.py"

}

process FilterRsIdList {

    publishDir "${preprocessingDir}/recodedVCFs/chrom${chromosome}", 
        mode: 'copy',
        overwrite: true

    input:
        val preprocessingDir
        tuple val(chromosome), val(sampleID), path(vcf), path(vcf_index), path(pair_list)

    output:
        tuple val(chromosome), val(filter_sampleID), path(filter_vcf), path(filter_vcf_index) 

    // when: 
    // "${pairStatus}" == "Filter"

    shell:
        filter_sampleID = "${sampleID}_ATCG_exluded"
        filter_vcf = "${sampleID}_ATCG_exluded.vcf.gz"
        filter_vcf_index = "${sampleID}_ATCG_exluded.vcf.gz.tbi"

        template 'FilterRsIdList.sh'

    // if (!{pairStatus} == "Filter"){
    //    template 'FilterRsIdList.sh'
    // }else{
    //    template 'notFilterRsIdList.sh'
    // }
 

}