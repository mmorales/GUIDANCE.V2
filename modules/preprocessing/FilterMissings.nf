process GetFrecuenciesVCF {

    publishDir "${preprocessingDir}/recodedVCFs/chrom${chromosome}", 
        mode: 'copy',
        overwrite: true

    input:
    path plinkBin
    val preprocessingDir
    tuple val(chromosome), val(sampleID), path(vcf), path(vcf_index) 

    output:
    tuple val(chromosome), path(frqx), emit: frqx
    path logs
    path nosex

    shell:
    frqx = "${sampleID}.frqx"
    logs = "${sampleID}.log"
    nosex = "${sampleID}.nosex"

    template 'GetFrequenciesVCF.sh'

}

process MissingIDsList {

    publishDir "${preprocessingDir}/recodedVCFs/chrom${chromosome}", 
        mode: 'copy',
        overwrite: true

    input:
    val preprocessingDir
    tuple val(chromosome), path(frqx)

    output:
    tuple val(chromosome), path("snps_to_filter_${chromosome}.txt")

    shell:
    template 'MissingIDsList.py'
 
}

process RemoveMissings {

    publishDir "${preprocessingDir}/recodedVCFs/chrom${chromosome}", 
        mode: 'copy',
        overwrite: true


    input:
    val preprocessingDir
    tuple val(chromosome), val(sampleID), path(vcf), path(vcf_index), path(missingRsList)


    output:
    tuple val(chromosome), val(filter_sampleID), path(filter_vcf), path(filter_vcf_index) 

    shell:
    filter_sampleID = "${sampleID}_missing_excluded"
    filter_vcf = "${sampleID}_missing_excluded.vcf.gz"
    filter_vcf_index = "${sampleID}_missing_excluded.vcf.gz.csi"

    template 'RemoveMissings.sh'

}