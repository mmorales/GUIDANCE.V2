process SplitChromosomes {

    maxForks 4

    publishDir "${preprocessingDir}/splited_chromosomes/chrom${chromosome}", 
        mode: 'copy',
        overwrite: true

    input:
    path plinkBin
    val preprocessingDir
    each chromosome
    tuple val (sampleID), path(bim), path(bed), path(fam) 

    output:
    tuple  val("${chromosome}"), val("${sampleID}_chr_${chromosome}"), path("${sampleID}_chr_${chromosome}.bim"), path("${sampleID}_chr_${chromosome}.bed"), path("${sampleID}_chr_${chromosome}.fam")

    shell:

    
    template 'SplitChromosomes.sh'


}