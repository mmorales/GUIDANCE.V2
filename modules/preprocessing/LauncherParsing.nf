process LauncherParsing {

    publishDir "${analysisDir}/launchers", 
        mode: 'copy',
        overwrite: true 

    input:
        val study
        val analysisDir
        val rootPath
        path json
        each chromsomeRange

    output:
        tuple val("${chromsomeRange}"), path("analysis_chr${chromsomeRange}*.cmd")
        
    shell:
        slurm_launcher = "analysis_chr${chromsomeRange}*.cmd"
        template 'LauncherParsing.py'

    
}