#!/usr/bin/env nextflow 


process LaunchersExecution {

    input:
    path nextflowBin
    val preprocessingDir
    tuple val(chromosome), val(sampleID), path(vcf), path(vcf_index), path(slurm_script)
    // tuple val(chr), path(slurm_script)
    // tuple val(chromosome), val(sampleID), path(vcf), path(vcf_index)


    shell:
    // print("${slurm_script}")
    template "LaunchersExecution.sh"


}