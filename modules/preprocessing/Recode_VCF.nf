process Recode_VCF {

    publishDir "${preprocessingDir}/recodedVCFs/chrom${chromosome}", 
        mode: 'copy',
        overwrite: true


    input:
        path plinkBin
        val preprocessingDir
        tuple val(chromosome), val(sampleID), path(bim), path(bed), path(fam)

    output:
        tuple val(chromosome), val(sampleID), path("${vcf}"), path("${vcf_index}")

    shell:
        vcf = "${sampleID}.vcf.gz"
        vcf_index = "${sampleID}.vcf.gz.tbi"
        template 'Recode_vcf.sh'

}