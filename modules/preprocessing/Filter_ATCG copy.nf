process CreateRsIdList {

    publishDir "${recodedVCFDir}", 
        mode: 'copy',
        overwrite: true

    input:
    val recodedVCFDir
    path bim
    //tuple val(sampleID), path(bim), path(bed), path(fam)

    output:
    path 'GCAT_IDs_list.pairs'

    shell:
    template "CreateRsIdList.py"
    

}

process FilterRsIdList {

    publishDir "${recodedVCFDir}", 
        mode: 'copy',
        overwrite: true

    input:
    val recodedVCFDir
    //path pair_list.filter{ file -> !file.isEmpty("test content qfile") }
    path pair_list
    tuple val(sampleID),path(vcf), path(vcf_index) 

    output:
    tuple val("exluded_${sampleID}") ,path("exluded_${vcf}"), path("exluded_${vcf_index}") 

    when: 
    pair_list.size() > 0

    shell:
    template 'FilterRsIdList.sh'

}