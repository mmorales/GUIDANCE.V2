process CheckStrand1 {

    publishDir "${preprocessingDir}/logs_qc_for_guidance", 
        mode: 'copy',
        overwrite: true
    
    input:
    val mainDir
    val studyPath
    val preprocessingDir
    tuple val(sampleID), path(bed), path(bim), path(fam) 

    output:
    val (sampleID), emit: sampleID
    tuple val("HRC_files"), path("*-HRC.txt"), emit: hrc_files
    path "*.sh", emit: run_plink

    shell:
    template 'CheckStrand1.sh' 
    
}


process CheckStrand2 {

    publishDir "${preprocessingDir}/QC_forGuidance", 
        mode: 'copy',
        overwrite: true 
    
    input:
    val sampleID
    tuple val("HRC_files"), path(hrc_files)
    path run_plink
    val preprocessingDir

    output:
    tuple val("${plink_strand}"), path("${plink_strand}.bim"), path("${plink_strand}.bed"), path("${plink_strand}.fam")

    shell:
    plink_strand="${sampleID}_forGuidance_hg19_strplus"
    template 'CheckStrand2.sh' 
    
}