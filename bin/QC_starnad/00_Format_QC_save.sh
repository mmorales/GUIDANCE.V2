#!/bin/bash

############ set

input=$1
IFS='/'
read -ra ARR <<< "$input"
plink_prefix=${ARR[-1]}
IFS=''
input_path=${input%/$plink_prefix}
work_path=$2
new_plink_prefix1=$plink_prefix"_1"
new_plink_prefix2=$plink_prefix"_2"
new_plink_prefix3=$plink_prefix"_3"
final_plink_prefix=$plink_prefix"_forGuidance"
plink_liftover=$final_plink_prefix"_hg19"
plink_strand=$plink_liftover"_strplus"
log_dir=$3
hg=$4

plink="/home/bsc64/bsc64212/apps/plink1.9/plink"

#cp $input_path/inds.missing.pheno.txt $work_path
#cp $input_path/$plink_prefix"_mod.fam" $work_path

# Remove SNPs with no physical position

awk '{if ($4==0) {print $2}}' $input.bim > $work_path"/nomap.snp"

plink1=`$plink --bfile $input --exclude $work_path/nomap.snp --make-bed --out $work_path/$new_plink_prefix1`


# Remove .hh SNPs

if [ -e $work_path"/"$new_plink_prefix1.hh ]; then
	awk '{print $3}' $work_path"/"$new_plink_prefix1.hh > $work_path/hh.snp
	sort $work_path/hh.snp | uniq  > $work_path/hh.snp.unique
	plink2=`$plink --bfile $work_path/$new_plink_prefix1 --exclude $work_path/hh.snp.unique --make-bed --out $work_path/$new_plink_prefix2`
fi

# Remove inds with missing pheno

if [ -e $input_path"/inds.missing.pheno.txt" ]; then
	if [ -e $work_path"/"$new_plink_prefix2.bim ]; then
		plink3=`$plink --bfile $work_path/$new_plink_prefix2 --remove $input_path/inds.missing.pheno.txt --make-bed --out $work_path/$new_plink_prefix3`
	else 
		plink3=`$plink --bfile $work_path/$new_plink_prefix1 --remove $input_path/inds.missing.pheno.txt --make-bed --out $work_path/$new_plink_prefix3`
	fi
fi

# Adjust fam if necessary

if [ -e $input_path"/"$plink_prefix"_mod.fam" ]; then
	if [ -e $work_path"/"$new_plink_prefix3.bim ]; then
		plink4=`$plink --bed $work_path/$new_plink_prefix3.bed --bim $work_path/$new_plink_prefix3.bim --fam $input_path/$plink_prefix.mod.fam --make-bed --out $work_path/$final_plink_prefix`
	elif [ -e $work_path"/"$new_plink_prefix2.bim ]; then
		plink4=`$plink --bed $work_path/$new_plink_prefix2.bed --bim $work_path/$new_plink_prefix2.bim --fam $input_path/$plink_prefix.mod.fam --make-bed --out $work_path/$final_plink_prefix`
	else
		plink4=`$plink --bed $work_path/$new_plink_prefix1.bed --bim $work_path/$new_plink_prefix1.bim --fam $input_path/$plink_prefix.mod.fam --make-bed --out $work_path/$final_plink_prefix`
	fi
fi


#### HOMOGENIZE OUTPUT

if [ ! -f $work_path"/"$final_plink_prefix.bim ]; then
	echo "no final"
	if [ -f $work_path"/"$new_plink_prefix3.bim ]; then
		echo "3"
		plink5=`$plink --bfile $work_path/$new_plink_prefix3 --make-bed --out $work_path/$final_plink_prefix`
	elif [ -f $work_path"/"$new_plink_prefix2.bim ]; then
		echo "2"
		plink6=`$plink --bfile $work_path/$new_plink_prefix2 --make-bed --out $work_path/$final_plink_prefix`
	else
		plink7=`$plink --bfile $work_path/$new_plink_prefix1 --make-bed --out $work_path/$final_plink_prefix`
	fi
fi

echo "Initial QC done"
echo "Final output is $work_path/$final_plink_prefix"


#####  Liftover

liftover_wrap=/gpfs/projects/bsc05/cecilia/02_QC_for_guidance_2/liftOverPlink-master/liftOverPlink.py
#chain=/gpfs/projects/bsc05/cecilia/02_QC_for_guidance/liftOverPlink-master/hg18ToHg19.over.chain.gz
liftover=/gpfs/projects/bsc05/cecilia/02_QC_for_guidance_2/liftover/liftOver

if [ $hg != "hg19" ]; then
	# create ped map files
	plink8=`$plink --bfile $work_path/$final_plink_prefix --recode --out $work_path/$final_plink_prefix`
	#convert 23 to X
        sed -i 's/^23\t/X\t/g' $work_path/$final_plink_prefix.map
	# liftover with plink wrapper
	if [ $hg == "hg18" ]; then
		chain=/gpfs/projects/bsc05/cecilia/02_QC_for_guidance_2/liftOverPlink-master/hg18ToHg19.over.chain.gz
	elif [ $hg == "hg17" ]; then
		 chain=/gpfs/projects/bsc05/cecilia/02_QC_for_guidance_2/liftOverPlink-master/hg17ToHg19.over.chain.gz
	fi 
	liftover1=`$liftover_wrap -m $work_path/$final_plink_prefix.map -p $work_path/$final_plink_prefix.ped -o $work_path/$plink_liftover -c $chain --bin $liftover`
	# back to plink binary
	plink9=`$plink --file $work_path/$plink_liftover --make-bed --out $work_path/$plink_liftover`
	#back to 23
	sed -i 's/^X\t/23\t/g' $work_path/$plink_liftover.bim
	
	echo "Liftover done"
	
else
	echo "Liftover not required"
fi


##### Strand check and map to +

module load perl/5.26

perl_strand=/gpfs/projects/bsc05/cecilia/02_QC_for_guidance_2/HRC-1000G-check-bim-v4.2.11-NoReadKey/HRC-1000G-check-bim-NoReadKey.pl
hrc_tab_ref=/gpfs/projects/bsc05/cecilia/02_QC_for_guidance_2/HRC.r1-1.GRCh37.wgs.mac5.sites.tab

if [ ! -f $work_path"/"$plink_liftover.bim ]; then
	echo "no lift"
	#calculate freq with plink
	plink10=`$plink --bfile $work_path/$final_plink_prefix --freq --out $work_path/$final_plink_prefix`
	#strand check with perl
	perl1=`perl $perl_strand --bim $work_path/$final_plink_prefix.bim --frequency $work_path/$final_plink_prefix.frq --ref $hrc_tab_ref --hrc --threshold 0.2`
else
	#calculate freq with plink
	plink10=`$plink --bfile $work_path/$plink_liftover --freq --out $work_path/$plink_liftover`
	#strand check with perl
        perl1=`perl $perl_strand --bim $work_path/$plink_liftover.bim --frequency $work_path/$plink_liftover.frq --ref $hrc_tab_ref --hrc --threshold 0.2`
fi

grep TEMP $log_dir/Run-plink.sh > $work_path/Run-plink_strand_corr.sh
mv $log_dir/*-HRC.txt $work_path
if [ $hg != "hg19" ]; then
	sed -i 's/_hg19-updated/_hg19_strplus/' $work_path/Run-plink_strand_corr.sh
else
	sed -i 's/-updated/_hg19_strplus/' $work_path/Run-plink_strand_corr.sh
fi
sed -i 's/plink/\/gpfs\/projects\/bsc05\/cecilia\/01a_QC_lorena_Cmod\/plink_1.9\/plink/' $work_path/Run-plink_strand_corr.sh
#sed -i 's/--bfile /--bfile"$work_

cd $work_path
bash $work_path/Run-plink_strand_corr.sh

echo "Strand correction done"
echo "Final output $work_path/$plink_strand"

