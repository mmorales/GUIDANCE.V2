#!/bin/bash
#SBATCH --job-name=next
#SBATCH --workdir=/gpfs/projects/bsc64/bsc64212/guidance.v2/outputs/phs000346.v2.p2/preprocessing/logs_qc_for_guidance
#SBATCH --output=/gpfs/projects/bsc64/bsc64212/guidance.v2/slurm_logs/%x_%j.out
#SBATCH --error=/gpfs/projects/bsc64/bsc64212/guidance.v2/slurm_logs/%x_%j.err
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=48
#SBATCH --qos=debug
#SBATCH --time=02:00:00
#SBATCH --exclusive

module load python
NXF_OPTS="-Dleveldb.mmap=false" /gpfs/projects/bsc64/bsc64212/guidance.v2/bin/nextflow/nextflow run /gpfs/projects/bsc64/bsc64212/guidance.v2/main.nf -c /gpfs/projects/bsc64/bsc64212/guidance.v2/profiles.config -profile local_binaries -resume -entry PREPROCESSING_DATA --with-report /gpfs/projects/bsc64/bsc64212/guidance.v2/slurm_logs/report -with-trace -with-timeline /gpfs/projects/bsc64/bsc64212/guidance.v2/slurm_logs/timeline -with-dag /gpfs/projects/bsc64/bsc64212/guidance.v2/slurm_logs/flowchart.png -w /gpfs/projects/bsc64/bsc64212/guidance.v2/work --pipeline PREPROCESSING_DATA



