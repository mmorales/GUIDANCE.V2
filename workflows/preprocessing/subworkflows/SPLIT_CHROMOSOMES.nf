#!/usr/bin/env nextflow

import java.nio.file.Paths

include { SplitChromosomes } from "${params.paths.preproModulesPath}/SplitChromosomes.nf"



workflow SPLIT_CHROMOSOMES {

    take:
        preprocessingDir
        newPlinkFiles

    main:

        def plinkBin    = params.tools.plinkBin

        Channel.of(params.init_chrom..params.end_chrom).set { chromsomeRange }

        SplitChromosomes(plinkBin, preprocessingDir, chromsomeRange, newPlinkFiles)
        splitedPlinkFiles = SplitChromosomes.out

    emit: 
        splitedPlinkFiles
}
