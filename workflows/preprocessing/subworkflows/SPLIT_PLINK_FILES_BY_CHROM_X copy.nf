#!/usr/bin/env nextflow 

include { SplitPlinkFilesByChromX as NewPlinkFilesFemale } from "${params.paths.preproModulesPath}/SplitPlinkFilesByChromX.nf"
include { SplitPlinkFilesByChromX as NewPlinkFilesMale  } from "${params.paths.preproModulesPath}/SplitPlinkFilesByChromX.nf"

workflow SPLIT_PLINK_FILES_BY_CHROM_X {

    take:
        plinkBin
        preprocessingDir
        newPlinkFiles

    main:

        def newPlinkFilesFemale = null
        def newPlinkFilesMale   = null 
        
        if (params.end_chrom == 23 | params.end_chrom == 'X' | params.end_chrom == 'x') {
           
            //
            NewPlinkFilesFemale("females", plinkBin, preprocessingDir, newPlinkFiles)
            //
            NewPlinkFilesMale("males", plinkBin, preprocessingDir, newPlinkFiles)

        }

    emit: 
        newPlinkFilesFemales = NewPlinkFilesFemale.out
        newPlinkFilesMales   = NewPlinkFilesMale.out

}