include { Recode_VCF as AllRecodeVCF } from "${params.paths.preproModulesPath}/Recode_VCF.nf"
include { Recode_VCF as FemaleRecodeVCF } from "${params.paths.preproModulesPath}/Recode_VCF.nf"
include { Recode_VCF as MaleRecodeVCF } from "${params.paths.preproModulesPath}/Recode_VCF.nf"

workflow RECODE_VCF {

    take:
        preprocessingDir
        splitedPlinkFiles
        plinkFilesFemales
        plinkFilesMales

    main:
        def plinkBin = params.tools.plinkBin
        
        AllRecodeVCF(plinkBin, preprocessingDir, splitedPlinkFiles)
        allRecodedVCF = AllRecodeVCF.out

        
        FemaleRecodeVCF(plinkBin, preprocessingDir, plinkFilesFemales)
        femaleRecodedVCF = FemaleRecodeVCF.out
        

        MaleRecodeVCF(plinkBin, preprocessingDir, plinkFilesMales)
        maleRecodedVCF = MaleRecodeVCF.out
        


    emit:
        allRecodedVCF 
        femaleRecodedVCF
        maleRecodedVCF

}