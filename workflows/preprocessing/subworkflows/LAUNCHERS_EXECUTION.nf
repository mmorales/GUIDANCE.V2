#!/usr/bin/env nextflow 

//include { LaunchersExecution } from "${params.paths.preproModulesPath}/LaunchersExecution.nf"
include { LaunchersExecution as AutoLaunchAnalysis   } from "${params.paths.preproModulesPath}/LaunchersExecution.nf"
include { LaunchersExecution as FemaleLaunchAnalysis } from "${params.paths.preproModulesPath}/LaunchersExecution.nf"
include { LaunchersExecution as MaleLaunchAnalysis   } from "${params.paths.preproModulesPath}/LaunchersExecution.nf"


workflow LAUNCHERS_EXECUTION {

    take: 
        preprocessingDir
        launchers
        filtered_missings_AutoVCF 
        filtered_missings_FemaleVCF 
        filtered_missings_MaleVCF

    main:

        def nextflowBin = params.tools.nextflowBin

        launchers.branch{ chrom, cmd -> 

                     def extension = cmd.name.split('_')[2]

                            all: extension == "all.cmd"
                                   return  tuple (chrom, cmd)
                            male: extension == "male.cmd"
                                   return  tuple (chrom, cmd)
                            female: extension == "female.cmd"
                                   return  tuple (chrom, cmd)
                                   
              }.set {slurm_launcher}


        filtered_missings_AutoVCF | join(slurm_launcher.all) 
                                  | set { allForLaunch }

        filtered_missings_FemaleVCF | join(slurm_launcher.female) 
                                    | set { femaleForLaunch }

        filtered_missings_MaleVCF | join(slurm_launcher.male) 
                                  | set { maleForLaunch }


        AutoLaunchAnalysis(nextflowBin, preprocessingDir, allForLaunch)
    
        FemaleLaunchAnalysis(nextflowBin, preprocessingDir,femaleForLaunch)

        MaleLaunchAnalysis(nextflowBin, preprocessingDir, maleForLaunch)

        // LaunchersExecution(nextflowBin, preprocessingDir, launchers, filtered_missings_AutoVCF)
    
        // FemaleLaunchAnalysis(nextflowBin, preprocessingDir, launchers, filtered_missings_FemaleVCF)

        // MaleLaunchAnalysis(nextflowBin, preprocessingDir, launchers, filtered_missings_MaleVCF)

}