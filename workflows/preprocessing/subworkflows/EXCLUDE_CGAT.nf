
include { CreateRsIdList; FilterRsIdList } from "${params.paths.preproModulesPath}/Exclude_CGTA.nf"
include { FilterRsIdList as  FilterIdListFemale} from "${params.paths.preproModulesPath}/Exclude_CGTA.nf"
include { FilterRsIdList as  FilterRsIdListMale} from "${params.paths.preproModulesPath}/Exclude_CGTA.nf"


workflow EXCLUDE_CGAT {

    take:


    main:

        CreateRsIdList()
        FilterRsIdList()
        

        if (params.end_chrom == '23' | params.end_chrom.toUpperCase() == 'X') {

            //
            FilterIdListFemale()
            FilterRsIdListMale()


        }

    
}