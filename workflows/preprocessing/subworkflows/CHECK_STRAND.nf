#!/usr/bin/env nextflow 

include { CheckStrand1; CheckStrand2 } from "${params.paths.preproModulesPath}/CheckStrand.nf"

workflow CHECK_STRAND {

    take:
        studyPath
        preprocessingDir 
        plinkFiles
    
    main:

        def mainDir = params.paths.guidanceRootPath

        // CheckStrand.nf
        CheckStrand1(mainDir,studyPath,preprocessingDir, plinkFiles)

        sampleID  = CheckStrand1.out.sampleID
        hrc_files = CheckStrand1.out.hrc_files
        run_plink = CheckStrand1.out.run_plink

        CheckStrand2(sampleID, hrc_files, run_plink, preprocessingDir)
        
        // Include validation of the results
        // Include results in log

    emit:
        newPlinkFiles = CheckStrand2.out

}