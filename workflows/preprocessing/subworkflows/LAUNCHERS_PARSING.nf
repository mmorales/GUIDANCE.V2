#!/usr/bin/env nextflow 



include { LauncherParsing } from "${params.paths.preproModulesPath}/LauncherParsing.nf"

workflow LAUNCHERS_PARSING {

    take:
        study
        preprocessingDir
        chromsomeRange

    main:

        def rootPath = params.paths.guidanceRootPath
        Channel.fromPath("${params.paths.configPath}/chromCPUs.json")
               | set { json } 

        LauncherParsing(study, preprocessingDir, rootPath, json, chromsomeRange)
        launchers = LauncherParsing.out.transpose()

        // launchers.map{ chrom, cmd ->

        //     if (cmd.name.endsWith('_all.cmd')){    [chrom, cmd, 'all'] }

        //     if (cmd.name.endsWith('_female.cmd')){ [chrom, cmd, 'female'] }

        //     if (cmd.name.endsWith('_male.cmd')){   [chrom, cmd, 'male'] }

        // }.set {slurm_launcher}
        
    emit:
        launchers

}
