#!/usr/bin/env nextflow 

include { SplitSampleFileByChromX as SampleFileFemale } from "${params.paths.preproModulesPath}/SplitSampleFIleByChromX.nf"
include { SplitSampleFileByChromX as SampleFileMale  } from "${params.paths.preproModulesPath}/SplitSampleFIleByChromX.nf"


workflow SPLIT_SAMPLE_FILES_BY_CHROM_X {

    take:
        sampleFile
        preprocessingDir
        plinkFilesFemale
        plinkFilesMale

    main:

        //
        SampleFileFemale("females", sampleFile, preprocessingDir, plinkFilesFemale)
        sampleFileFemale = SampleFileFemale.out

        //
        SampleFileMale("males", sampleFile, preprocessingDir, plinkFilesMale)
        sampleFileMale = SampleFileMale.out

    emit:
        sampleFileFemale
        sampleFileMale

}           