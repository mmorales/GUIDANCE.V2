include { GetFrecuenciesVCF } from "${params.paths.preproModulesPath}/FilterMissings.nf"
include { MissingIDsList } from "${params.paths.preproModulesPath}/FilterMissings.nf"


include { RemoveMissings as AutosomalRemoveMissings } from "${params.paths.preproModulesPath}/FilterMissings.nf"
include { RemoveMissings as FemaleRemoveMissings } from "${params.paths.preproModulesPath}/FilterMissings.nf"
include { RemoveMissings as MaleRemoveMissings } from "${params.paths.preproModulesPath}/FilterMissings.nf"


workflow FILTER_MISSINGS {

    take: 
        preprocessingDir
        filtered_ATCG_AutoVCF
        filtered_ATCG_FemaleVCF
        filtered_ATCG_MaleVCF

    main:

        def plinkBin = params.tools.plinkBin

        GetFrecuenciesVCF(plinkBin, preprocessingDir, filtered_ATCG_AutoVCF) 
        frqx = GetFrecuenciesVCF.out.frqx

        MissingIDsList(preprocessingDir, frqx)
        missingSNPs = MissingIDsList.out

        filtered_ATCG_AutoVCF \
                | join(missingSNPs) \
                | set { filtered_ATCG_AutoVCF_to_filter }

        filtered_ATCG_FemaleVCF \
                | join(missingSNPs) \
                | set { filtered_ATCG_FemaleVCF_to_filter }

        filtered_ATCG_MaleVCF \
                | join(missingSNPs) \
                | set { filtered_ATCG_MaleVCF_to_filter }

        AutosomalRemoveMissings(preprocessingDir, filtered_ATCG_AutoVCF_to_filter)
        filtered_missings_AutoVCF = AutosomalRemoveMissings.out

        FemaleRemoveMissings(preprocessingDir, filtered_ATCG_FemaleVCF_to_filter)
        filtered_missings_FemaleVCF = FemaleRemoveMissings.out

        MaleRemoveMissings(preprocessingDir, filtered_ATCG_MaleVCF_to_filter)
        filtered_missings_MaleVCF = MaleRemoveMissings.out 

    emit:
        filtered_missings_AutoVCF
        filtered_missings_FemaleVCF
        filtered_missings_MaleVCF

}