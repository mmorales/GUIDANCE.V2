#!/usr/bin/env nextflow 

include { SplitPlinkFilesByChromX as PlinkFilesFemale } from "${params.paths.preproModulesPath}/SplitPlinkFilesByChromX.nf"
include { SplitPlinkFilesByChromX as PlinkFilesMale  } from "${params.paths.preproModulesPath}/SplitPlinkFilesByChromX.nf"

workflow SPLIT_PLINK_FILES_BY_CHROM_X {

    take:
        preprocessingDir
        inputsPlinkFiles

    main:

        def plinkBin = params.tools.plinkBin
        
        //
        PlinkFilesFemale("females", plinkBin, preprocessingDir, inputsPlinkFiles)
        plinkFilesFemales = PlinkFilesFemale.out
        
        //
        PlinkFilesMale("males", plinkBin, preprocessingDir,inputsPlinkFiles)
        plinkFilesMales = PlinkFilesMale.out

    emit: 
        plinkFilesFemales
        plinkFilesMales   
}
