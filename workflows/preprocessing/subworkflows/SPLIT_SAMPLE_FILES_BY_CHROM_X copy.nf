#!/usr/bin/env nextflow 

include { SplitSampleFileByChromX as NewSampleFileFemale } from "${params.paths.preproModulesPath}/SplitSampleFIleByChromX.nf"
include { SplitSampleFileByChromX as NewSampleFileMale  } from "${params.paths.preproModulesPath}/SplitSampleFIleByChromX.nf"


workflow SPLIT_SAMPLE_FILES_BY_CHROM_X {

    take:
        sampleFile
        preprocessingDir
        newPlinkFilesFemale
        newPlinkFilesMale

    main:

        if (params.end_chrom == 23 | params.end_chrom == 'X' | params.end_chrom == 'x') {

            newSampleFileFemale = NewSampleFileFemale("females", sampleFile, preprocessingDir, newPlinkFilesFemale)

            newSampleFileMale   = NewSampleFileMale("males", sampleFile, preprocessingDir, newPlinkFilesMale)

        }

    emit:
        newSampleFileFemale
        newSampleFileMale

}           