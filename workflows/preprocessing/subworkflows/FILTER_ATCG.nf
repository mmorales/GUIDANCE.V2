include { CreateRsIdList } from "${params.paths.preproModulesPath}/Filter_ATCG.nf"

include { FilterRsIdList as AutosomalFilterRsIdList } from "${params.paths.preproModulesPath}/Filter_ATCG.nf"
include { FilterRsIdList as FemaleFilterRsIdList } from "${params.paths.preproModulesPath}/Filter_ATCG.nf"
include { FilterRsIdList as MaleFilterRsIdList } from "${params.paths.preproModulesPath}/Filter_ATCG.nf"

def check_empty_pair(Path pair_file) {

    if ( pair_file.readLines().isEmpty() ){

        filterStatus = "notFilter"
        print("[ FILTER_ATCG ]: the pair file is empty. Not AT/CG variants founded.")

    } else {

        filterStatus = "Filter"
    }

    return filterStatus

}

workflow FILTER_ATCG {

    take:
        newPlinkFiles
        preprocessingDir
        autoRecodedVCF
        femaleRecodedVCF
        maleRecodedVCF

    main:

        if (params.exclude_cgat_snps == true){

            CreateRsIdList(preprocessingDir, newPlinkFiles)
            pair_list = CreateRsIdList.out

            // pair_list  \
            //     | map{ check_empty_pair(it) } \
            //     | set{ statusPair }

            autoRecodedVCF \
                | combine(pair_list) \
                | set { autoRecodedVCF_to_filter }

            femaleRecodedVCF \
                | combine(pair_list) \
                | set { femaleRecodedVCF_to_filter }

            maleRecodedVCF \
                | combine(pair_list) \
                | set { maleRecodedVCF_to_filter }

        
            AutosomalFilterRsIdList(preprocessingDir, autoRecodedVCF_to_filter)
            filtered_ATCG_AutoVCF = AutosomalFilterRsIdList.out
           
            FemaleFilterRsIdList(preprocessingDir, femaleRecodedVCF_to_filter)
            filtered_ATCG_FemaleVCF = FemaleFilterRsIdList.out

            MaleFilterRsIdList(preprocessingDir, maleRecodedVCF_to_filter)
            filtered_ATCG_MaleVCF = MaleFilterRsIdList.out
        
        }

    emit:     
        filtered_ATCG_AutoVCF
        filtered_ATCG_FemaleVCF
        filtered_ATCG_MaleVCF

}




   

