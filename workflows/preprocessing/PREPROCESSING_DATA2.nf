#!/usr/bin/env nextflow 

import java.nio.file.Paths

include { CHECK_STRAND                  } from './subworkflows/CHECK_STRAND.nf'
include { SPLIT_CHROMOSOMES             } from './subworkflows/SPLIT_CHROMOSOMES.nf'
include { SPLIT_PLINK_FILES_BY_CHROM_X  } from './subworkflows/SPLIT_PLINK_FILES_BY_CHROM_X.nf'
include { SPLIT_SAMPLE_FILES_BY_CHROM_X } from './subworkflows/SPLIT_SAMPLE_FILES_BY_CHROM_X.nf'
include { RECODE_VCF                    } from './subworkflows/RECODE_VCF.nf'
include { FILTER_ATCG                   } from './subworkflows/FILTER_ATCG.nf'
include { FILTER_MISSINGS               } from './subworkflows/FILTER_MISSINGS.nf'
include { LAUNCHERS_PARSING             } from './subworkflows/LAUNCHERS_PARSING.nf'
include { LAUNCHERS_EXECUTION           } from './subworkflows/LAUNCHERS_EXECUTION.nf'


workflow PREPROCESSING {


       // def step_range = Utils.stepsValidation(params, log) 
       
       // def from_step = step_range[0] as Integer
       // def unit_step = step_range[1] as Integer


       def until_step = Utils.stepsValidation(params, log)

       def cohortsDir  = params.cohort.cohortsDir
       def study       = params.cohort.study
       Path studyPath  = Paths.get(cohortsDir, study)

       def outputDir   = params.outputDir
       Path outStudy   = Paths.get(outputDir, study)

       Path preprocessingDir = outStudy.resolve("preprocessing")
      


       // INPUT CHANNELS

       Channel.fromFilePairs("${studyPath}/*.{bed,fam,bim}", size:3, flat:true) \
              | set { plinkFiles }

    
       Channel.fromPath("${studyPath}/*.sample") \
              | set { sampleFile }


       Channel.of(params.init_chrom..params.end_chrom) \
           | set { chromsomeRange }



       // PREPROCESSING STEPS:

       if (until_step >=  0){
              log.info "Executing step 1: Check_Strand"

              CHECK_STRAND(studyPath, preprocessingDir, plinkFiles)
              newPlinkFiles = CHECK_STRAND.out
             
       }


       if (until_step >=  1){
              log.info "Executing step 2: Split_Chromosomes"

              SPLIT_CHROMOSOMES(preprocessingDir, newPlinkFiles)
              splitedPlinkFiles = SPLIT_CHROMOSOMES.out

              splitedPlinkFiles.branch{ chrom, sampleid, bim, bed, fam ->
                        sexChrom: chrom == '23'
                        return tuple (chrom, sampleid, bim, bed, fam)
                        autosomChrom: chrom != '23'
                        return tuple (chrom, sampleid, bim, bed, fam)
              }.set{ inputsPlinkFiles }
             
       }

       if (until_step >=  2){
              log.info "Executing step 3: Check_Chrom_X"

              SPLIT_PLINK_FILES_BY_CHROM_X(preprocessingDir, inputsPlinkFiles.sexChrom) 
              plinkFilesFemales = SPLIT_PLINK_FILES_BY_CHROM_X.out.plinkFilesFemales
              plinkFilesMales   = SPLIT_PLINK_FILES_BY_CHROM_X.out.plinkFilesMales

              SPLIT_SAMPLE_FILES_BY_CHROM_X(sampleFile, preprocessingDir, plinkFilesFemales, plinkFilesMales)
              sampleFileFemale = SPLIT_SAMPLE_FILES_BY_CHROM_X.out.sampleFileFemale
              sampleFileMale   = SPLIT_SAMPLE_FILES_BY_CHROM_X.out.sampleFileMale
       }

       if (until_step >=  3){
              log.info "Executing step 4: Recode_vcf"

              RECODE_VCF(preprocessingDir, splitedPlinkFiles, plinkFilesFemales, plinkFilesMales)
              allRecodedVCF   = RECODE_VCF.out.allRecodedVCF
              femaleRecodedVCF = RECODE_VCF.out.femaleRecodedVCF
              maleRecodedVCF   = RECODE_VCF.out.maleRecodedVCF
      
       }

       if (until_step >=  4){
              log.info "Executing step 5: Filter_ATCG_and_Missings"

              FILTER_ATCG(newPlinkFiles, preprocessingDir, allRecodedVCF, femaleRecodedVCF, maleRecodedVCF)
              filtered_ATCG_AutoVCF = FILTER_ATCG.out.filtered_ATCG_AutoVCF
              filtered_ATCG_FemaleVCF = FILTER_ATCG.out.filtered_ATCG_FemaleVCF
              filtered_ATCG_MaleVCF = FILTER_ATCG.out.filtered_ATCG_MaleVCF

              FILTER_MISSINGS(preprocessingDir, filtered_ATCG_AutoVCF, filtered_ATCG_FemaleVCF, filtered_ATCG_MaleVCF)
              filtered_missings_AutoVCF = FILTER_MISSINGS.out.filtered_missings_AutoVCF
              filtered_missings_FemaleVCF = FILTER_MISSINGS.out.filtered_missings_FemaleVCF
              filtered_missings_MaleVCF = FILTER_MISSINGS.out.filtered_missings_MaleVCF       

       }

       if (until_step >=  5){
              log.info "Executing step 6: parse_launcher_analysis"

              LAUNCHERS_PARSING(study, preprocessingDir, chromsomeRange)
              launchers = LAUNCHERS_PARSING.out.launchers

       }

       if (until_step >= 6){
              log.info "Executing step 7: launch_analysis"

              LAUNCHERS_EXECUTION(preprocessingDir, launchers, filtered_missings_AutoVCF, filtered_missings_FemaleVCF, filtered_missings_MaleVCF)

       }
      

}