#!/usr/bin/env nextflow 

import java.nio.file.Paths


include { CHECK_STRAND } from './subworkflows/CHECK_STRAND.nf'
//include { SPLIT_CHROMOSOMES } from './subworflows/SPLIT_CHROMOSOMES.nf'
include { SPLIT_PLINK_FILES_BY_CHROM_X }  from './subworkflows/SPLIT_PLINK_FILES_BY_CHROM_X.nf'
include { SPLIT_SAMPLE_FILES_BY_CHROM_X } from './subworkflows/SPLIT_SAMPLE_FILES_BY_CHROM_X.nf'

// include { EXCLUDE_CGAT }    from './subworkflows/EXCLUDE_CGAT.nf'
// include { RECODEVCF }       from './subworkflows/RECODEVCF.nf'
// include { REMOVE_MISSINGS } from './subworkflows/REMOVE_MISSINGS.nf'



workflow PREPROCESSING {

    def cohortsDir  = params.cohort.cohortsDir
    def study       = params.cohort.study
    Path studyPath  = Paths.get(cohortsDir, study)

    def outputDir   = params.outputDir
    Path outStudy   = Paths.get(outputDir, study)

    Path preprocessingDir = outStudy.resolve("preprocessing")
    Path recodedVCFDir    = preprocessingDir.resolve("recodedVCF")


    Channel.fromFilePairs("${studyPath}/*.{bed,fam,bim}", size:3, flat:true)
           .set { plinkFiles }

    Channel.fromPath("${studyPath}/*.sample")
           .set { sampleFile }

    main:
       println(params.index)
       // until_step = 5
       // step=0

       // while(step < until_step){
       //        print("hola")
       //        step++
       // }

       // CHECK_STRAND(studyPath, preprocessingDir, plinkFiles)


       // newPlinkFiles = CHECK_STRAND.out

       // SPLIT_CHROMOSOMES(preprocessingDir, newPlinkFiles)
  
       //SPLIT_PLINK_FILES_BY_CHROM_X( plinkBin, preprocessingDir, newPlinkFiles ) 

       //newPlinkFilesFemales = SPLIT_PLINK_FILES_BY_CHROM_X.out.newPlinkFilesFemales
       //newPlinkFilesMales = SPLIT_PLINK_FILES_BY_CHROM_X.out.newPlinkFilesMales

       //SPLIT_SAMPLE_FILES_BY_CHROM_X(sampleFile, preprocessingDir, newPlinkFilesFemales, newPlinkFilesMales)
   
}



    // if (params.wfDeep == 'until_CheckStrand') {

    //     newPlinkFiles = UNTIL_CHECKSTRAND(studyPath, preprocessingDir, plinkFiles)
    // }

    // if (params.wfDeep == 'until_CheckSex') {

    //     newSexPlinkFiles = UNTIL_CHECKSEX(plinkBin, preprocessingDir, newPlinkFiles) 
        
    //     femalePlinkFiles = newSexPlinkFiles.out.femaleFiles
    //     malePlinkFiles   = newSexPlinkFiles.out.maleFiles
    // }

// }
