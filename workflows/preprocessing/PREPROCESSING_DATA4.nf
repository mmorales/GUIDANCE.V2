#!/usr/bin/env nextflow 

import java.nio.file.Paths

if (params.wfDeep == 'until_Check_Strand') {
    include { UNTIL_CHECK_STRAND } from './UNTIL_CHECK_STRAND.nf'
}

if (params.wfDeep == 'until_Split_Chromosomes'){
    include { UNTIL_SPLIT_CHROMOSOMES } from './UNTIL_SPLIT_CHROMOSOMES.nf'
}

if (params.wfDeep == 'until_Check_Chrom_X') {
    include { UNTIL_CHECK_CHROM_X } from './UNTIL_CHECK_CHROM_X.nf'
}

 if (params.wfDeep == 'until_Recode_vcf') {
     include { UNTIL_RECODE_VCF } from './UNTIL_RECODE_VCF.nf'
}

 if (params.wfDeep == 'until_Filter_ATCG_and_Missings') {
     include { UNTIL_FILTER_ATCG_AND_MISSINGS } from './UNTIL_FILTER_ATCG_AND_MISSINGS.nf'
}

// if (params.wfDeep == 'until_Filter_and_Recode_vcf') {
//     include { UNTIL_FILTER_AND_RECODE_VCF } from './UNTIL_FILTER_AND_RECODE_VCF.nf'
// }

// if (params.wfDeep == 'until_AnalysisParser') {
//     include { UNTIL_ANALYSIS_PARSER } from './UNTIL_ANALYSIS_PARSER.nf'
// }



workflow PREPROCESSING {


    def plinkBin    = params.tools.plinkBin

    def cohortsDir  = params.cohort.cohortsDir
    def study       = params.cohort.study
    Path studyPath  = Paths.get(cohortsDir, study)

    def outputDir   = params.outputDir
    Path outStudy   = Paths.get(outputDir, study)

    Path preprocessingDir = outStudy.resolve("preprocessing")
    //Path recodedVCFDir    = preprocessingDir.resolve("recodedVCF")


    Channel.fromFilePairs("${studyPath}/*.{bed,fam,bim}", size:3, flat:true)
           .set { plinkFiles }

    
    Channel.fromPath("${studyPath}/*.sample")
           .set { sampleFile }


    if (params.wfDeep == 'until_Check_Strand') {

        UNTIL_CHECK_STRAND(studyPath, preprocessingDir, plinkFiles)
    }

    if (params.wfDeep == 'until_Split_Chromosomes'){
         
        UNTIL_SPLIT_CHROMOSOMES(studyPath, preprocessingDir, plinkFiles) 
    }

    if (params.wfDeep == 'until_Check_Chrom_X') {

        UNTIL_CHECK_CHROM_X(studyPath, preprocessingDir, plinkFiles, sampleFile) 
    }

    if (params.wfDeep == 'until_Recode_vcf') {

        UNTIL_RECODE_VCF(studyPath, preprocessingDir, plinkFiles, sampleFile)
    }

     if (params.wfDeep == 'until_Filter_ATCG_and_Missings') {
         
         UNTIL_FILTER_ATCG_AND_MISSINGS(studyPath, preprocessingDir, plinkFiles, sampleFile)
    }

    // if (params.wfDeep == 'until_Filter_and_Recode_vcf') {

    //     UNTIL_FILTER_AND_RECODE_VCF(plinkBin, studyPath, preprocessingDir, plinkFiles)
    // }

    // if (params.wfDeep == 'until_AnalysisParser') {

    //     UNTIL_ANALYSIS_PARSER(plinkBin, studyPath, preprocessingDir, plinkFiles)
    // }
}



  