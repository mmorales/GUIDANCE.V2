#!/usr/bin/env nextflow 

import java.nio.file.Paths

include { PHASING             } from './subworkflows/PHASING.nf'
include { IMPUTATION          } from './subworkflows/IMPUTATION.nf'
include { FILTER_MAF_AND_INFO } from './subworkflows/FILTER_MAF_AND_INFO.nf'
include { MERGING             } from './subworkflows/MERGING.nf'
include { ASSOCIATION_TEST    } from './subworkflows/ASSOCIATION_TEST.nf'



workflow ANALYSIS {


    def until_step        = Utils.stepsValidation(params, log) as Integer
    def cohortsDir        = params.cohort.cohortsDir
    def study             = params.cohort.study
    Path studyPath        = Paths.get(cohortsDir, study)

    def outputDir         = params.outputDir
    Path outStudy         = Paths.get(outputDir, study)

    Path preprocessingDir = outStudy.resolve("preprocessing")
    Path analysisDir      = outStudy.resolve("analysis")

    def tooltype          = params.tooltype
    def chromosome        = params.chromosome
    def CPUS              = params.CPUS
    def TIME              = params.TIME
    def chromtype         = params.chrom_type 


    if(chromtype == "all"){
        path = "${preprocessingDir}/recodedVCFs/chrom${chromosome}/phs*_missing_excluded.vcf.gz*"
    }else {
        path = "${preprocessingDir}/recodedVCFs/chrom${chromosome}/${chromtype}*_missing_excluded.vcf.gz*"
    }

    // Channel.fromPath(path) \
    //        | toList() \
    //        | map{ it -> 
    //             if (it[0].contains(".csi")) {
    //                 csi = it[0]
    //                 vcf = it[1]
    //                 [ chromosome , vcf , csi ]

    //             }else{
    //                 csi = it[1]
    //                 vcf = it[0]
    //                 [ chromosome , vcf , csi ]
    //             }
    //         }
    //        //| map { vcf, vcf_index -> tuple(chromosome, vcf, vcf_index) } \
    //         | set { vcfs }

    Channel.fromPath(path) \
           | toList() \
           | map { it -> 

                sampleid = it[0].baseName.split('\\.')[0]
                vcf = it.findAll { it.name.endsWith(".vcf.gz") } [0]
                csi = it.findAll { it.name.endsWith(".vcf.gz.csi") } [0]
        
                return [ chromosome, sampleid, vcf, csi ]
           
           }
           | set { vcfs }
    

    Channel.fromPath("${params.paths.mapsPath}/*_chr${chromosome}_*.txt.gz") \
            | map { it -> tuple(chromosome, it) } \
            | set{ map }
    

    if (until_step >= 7){
        log.info "Executing step 8: shapeit4"

        PHASING(chromtype, tooltype, analysisDir, vcfs, map, CPUS)
        phased_vcfs = PHASING.out
    }
    
    if (until_step >= 8){
        log.info "Executing step 9: impute5"
    
        IMPUTATION(chromtype, tooltype, chromosome, analysisDir, phased_vcfs, map, CPUS)
        imputedOnethousandG = IMPUTATION.out.imputedOnethousandG
        imputedGonl = IMPUTATION.out.imputedGonl

    }

    imputedOnethousandG.view()

    if (until_step >= 9){
        log.info "Executing step 10: shapeit4"

        //
        // FILTER_MAF_AND_INFO(chromtype, tooltype, analysisDir, imputedOnethousandG, CPUS)

        // //
        // FILTER_MAF_AND_INFO(chromtype, tooltype, analysisDir, imputedGonl, CPUS)

    }
    
    // if (until_step >= 10){
    //     log.info "Executing step 11: imputation"

    //     // MERGE()

    // }



}