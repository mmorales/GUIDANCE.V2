#!/usr/bin/env nextflow 

include { Phasing } from"${params.paths.anaModulesPath}/Phasing.nf"

workflow PHASING {

    take:
        chromtype
        tooltype
        analysisDir
        vcfsToPhase
        map
        CPUS

    main:

        def shapeit4Bin = params.tools.shapeit4Bin

        Phasing(shapeit4Bin, chromtype, tooltype, analysisDir, vcfsToPhase, map, CPUS)
        phased_vcfs = Phasing.out.phased_file

    emit:
        phased_vcfs

        
}