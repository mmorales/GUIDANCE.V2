#!/usr/bin/env nextflow 

include { CoordinatesChunker as ChunkOnethousandG } from"${params.paths.anaModulesPath}/Imputation.nf"
include { CoordinatesChunker as ChunckGonl } from"${params.paths.anaModulesPath}/Imputation.nf"

include { Imputation as ImpOnethousandG } from"${params.paths.anaModulesPath}/Imputation.nf"
include { Imputation as ImpGonl } from"${params.paths.anaModulesPath}/Imputation.nf"


workflow IMPUTATION {

    take:
        chromtype
        tooltype
        chromosome
        analysisDir
        phased_vcfs
        map
        CPUS

    main:

    def refpanelNames = params.refpanel_names.split(';')
    def panelsDir     = params.paths.panelsPath

    def impute5Chuncker = params.tools.impute5Chuncker
    def impute5Bin = params.tools.impute5Bin

    Path imputeOnethousandGDir = analysisDir.resolve("imputation/onethousandG")
    Path imputeGonlDir = analysisDir.resolve("imputation/gonl")


    for(panelName in refpanelNames) {

        def panel = "${panelsDir}/${panelName}"

        if (panel.contains('onethousandG') ){
            // Channel.fromPath("${panel}/*.chr${chromosome}.*.vcf.gz*") \
            //     | toSortedList() \
            //     | map { it -> 

            //             if (it[0].contains(".csi")) {
            //                 csi = it[0]
            //                 vcf = it[1]
            //                 [ chromosome , vcf , csi ]

            //             }else{
            //                 csi = it[1]
            //                 vcf = it[0]
            //                 [ chromosome , vcf , csi ]
            //             }
            //         }
            //     | set { onethousandG }

            Channel.fromPath("${panel}/*.chr${chromosome}.*.vcf.gz*") \
                   | toSortedList() \
                   | map { it -> 

                        sampleid = it[0].baseName.split('\\.')[0]
                        vcf = it.findAll { it.name.endsWith(".vcf.gz") } [0]
                        csi = it.findAll { it.name.endsWith(".vcf.gz.csi") } [0]

                        return [ chromosome, sampleid, vcf, csi ]
                   }
                   | set { onethousandG }
            
            ChunkOnethousandG(impute5Chuncker, chromtype, tooltype, imputeOnethousandGDir, onethousandG, phased_vcfs, CPUS)
            
            def regionsOnethousandG  = ChunkOnethousandG.out.regions
            def regionsToImputeOTG   = regionsOnethousandG.splitCsv(sep:"\t")

            // map | join(onethousandG) | join(phased_vcfs) | set { baseInputsOTG }
            // baseInputsOTG | combine(regionsToImputeOTG) |  set { imputationInputsOTG }

            onethousandG | join(map) 
                         | join(phased_vcfs)
                         | combine(regionsToImputeOTG)
                         //| toSortedList( { a, b -> b[6] <=> a[6] } )
                         | set { imputationInputsOTG }

            //imputationInputsOTG.view()
            ImpOnethousandG(impute5Bin, chromtype, tooltype, imputeOnethousandGDir, CPUS, imputationInputsOTG)
            imputedOnethousandG = ImpOnethousandG.out.imputed_vcf

        }

        if (panel.contains('gonl') ){
            // Channel.fromPath("${panel}/*chr${chromosome}*.vcf.gz*") \
            //     | toSortedList() \
            //     | map { it -> 

            //             if (it[0].contains(".csi")) {
            //                 csi = it[0]
            //                 vcf = it[1]
            //                 [ chromosome , vcf , csi ]

            //             }else{
            //                 csi = it[1]
            //                 vcf = it[0]
            //                 [ chromosome , vcf , csi ]
            //             }
            //         }
            //     | set { gonl }

            //| map { vcf, csi -> tuple(chromosome, vcf_index, vcf) } \

            Channel.fromPath("${panel}/*chr${chromosome}*.vcf.gz*") \
                | toSortedList() \
                | map { it -> 

                        sampleid = it[0].baseName.split('\\.')[0]
                        vcf = it.findAll { it.name.endsWith(".vcf.gz") } [0]
                        csi = it.findAll { it.name.endsWith(".vcf.gz.csi") } [0]

                        return [ chromosome, sampleid, vcf, csi ]
                }
                | set { gonl }

            ChunckGonl(impute5Chuncker, chromtype, tooltype, imputeGonlDir, gonl, phased_vcfs, CPUS)

            def regionsGonl         = ChunckGonl.out.regions
            def regionsToImputeGonl = regionsGonl.splitCsv(sep:"\t")

            // map | join(gonl) | join(phased_vcfs) | set { baseInputsGonl }
            // baseInputsGonl | combine(regionsToImputeGonl) |  set { imputationInputsGonl }

            gonl | join(map) 
                 | join(phased_vcfs)
                 | combine(regionsToImputeGonl)
                 | set { imputationInputsGonl }

                 //| toSortedList( { a, b -> b[6] <=> a[6] } )

            //imputationInputsGonl.view()
            //imputationInputsGonl | count | set { numChunksGonl }

            ImpGonl(impute5Bin, chromtype, tooltype, imputeGonlDir, CPUS, imputationInputsGonl)
            imputedGonl = ImpGonl.out.imputed_vcf

        }

    }

    emit:
        imputedOnethousandG
        imputedGonl

}
