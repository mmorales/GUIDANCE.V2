#!/usr/bin/env nextflow 

include { filter_maf_and_info } from "${params.paths.anaModulesPath}/Filter_maf_and_info.nf"

workflow FILTER_MAF_AND_INFO {

    take:
        chromtype
        tooltype
        analysisDir
        imputed_vcfs
        CPUS

    main:

        def mafTheshold     = params.maf_threshold
        def imputeThreshold = params.impute_threshold




    // emit:

}